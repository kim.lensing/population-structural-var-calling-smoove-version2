#!/usr/bin/env Rscript
library(dplyr)
library(tidyr)

args = commandArgs(trailingOnly=TRUE)
qual_threshold = args[2]

vcf_table = read.table(file = args[1], sep = '\t', header = TRUE)

#filter dataset on quality
qual_filtered = vcf_table[vcf_table$QUAL> as.integer(qual_threshold),]

#filter high quality variants based on duphold
filtered_dataset = qual_filtered[qual_filtered$TYPE %in% "DEL" & qual_filtered$DHFFC_0.1 < 0.8 | qual_filtered$TYPE %in% "DUP" & qual_filtered$DHBFC_0.1 > 1.2 |  qual_filtered$TYPE %in% "INV",]
filtered_dataset = filtered_dataset %>% drop_na(CHR)

write.table(filtered_dataset, args[3] , sep = "\t",row.names = F)
