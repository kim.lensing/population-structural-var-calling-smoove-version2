import sys
import vcf
import pandas as pd
import csv
import argparse
import math



parser = argparse.ArgumentParser( description='Select candidate SV events')
parser.add_argument("-v", "--VCF", help="VCF_file with SV events", nargs=1, type=str)
parser.add_argument("-o","--OUTFILE",help="Output tsv file",nargs=1,type=str)
parser.add_argument("-s","--SAMPLES_TABLE",help="table with sample info. First column: sample; second column: bam file; third column: population",nargs=1,type=str)
args = parser.parse_args()

reader = vcf.Reader(filename = args.VCF[0])
samples_table = pd.read_csv(args.SAMPLES_TABLE[0], names = ["sample", "bam", "population"])

csvfile = open(args.OUTFILE[0], "w")
outfile = csv.writer(csvfile, delimiter='\t')

header = ["CHR", "POS", "TYPE", "SVLEN", "QUAL"]
populations = samples_table.population.unique()

population_headers = [p + "_freq" for p in populations]

complete_header = header + population_headers + ["GENE_SYMBOL", "ANNOTATION", "DHBFC_1/1", "DHBFC_0/1", "DHBFC_0/0", "DHFFC_1/1", "DHFFC_0/1", "DHFFC_0/0", "Homozygotes", "Heterozygotes"]
outfile.writerow(complete_header)
infos = reader.infos.keys()



for record in reader:
    row_to_write = [record.CHROM, record.POS, record.INFO.get("SVTYPE"), record.INFO.get("SVLEN")[0], record.QUAL]
    pop_gt_dict = {}
    for sample in record.samples:
        population = samples_table.loc[samples_table["sample"].str.startswith(sample.sample), "population"].item()
        if population in pop_gt_dict:
            pop_gt_dict[population].append(sample["GT"])
        else:
            pop_gt_dict[population] = [sample["GT"]]

    for key, value in pop_gt_dict.items():
        points = 0
        for el in value:
            if el == "1/1":
                points = points + 2
            elif el == "1/0" or el == "0/1":
                points = points + 1
            elif el == "0/0":
                continue
        freq = points / (len(value) * 2) * 100
        row_to_write.append(freq)

    csqs = record.INFO.get("smoove_gene")
    ann, symbol = [], []
    if csqs is not None:
        for csq in csqs:
            cs = csq.split(",")
            for it in cs:
                elem = it.split("|")
                anno = elem[1].split(":")[0]
            if elem[0] == "":
                elem[0] = "-"
            if anno not in ann:
                ann.append(anno+":"+elem[0])
            if elem[0] not in symbol:
                symbol.append(elem[0])
    row_to_write.append(",".join(symbol))
    row_to_write.append(",".join(ann))

    if "DEPTH" in infos:
        depth = record.INFO.get("DEPTH")
        [row_to_write.append(d) for d in depth]
    else:
        row_to_write.append("-")
    het_samples, hom_samples = [], []
    for hom in record.get_hom_alts():
        hom_samples.append(hom.sample)
    for het in record.get_hets():
        het_samples.append(het.sample)
    row_to_write.append(",".join(hom_samples))
    row_to_write.append(",".join(het_samples))
    outfile.writerow(row_to_write) 
    
csvfile.close()
