configfile: "config.yaml"

from snakemake.utils import makedirs
import pandas as pd

#################################
# author: Carolina Pita Barros  #
# carolina.pitabarros@wur.nl    #
# date: August 2021             #
#################################

if "OUTDIR" in config:
    # print("\nSaving to " + config["OUTDIR"] + "\n")
    workdir: config["OUTDIR"]

makedirs("logs_slurm")

pipeline = "population-structural-var-calling-smoove" 


include: "rules/create_file_log.smk"

REFERENCE=config["REFERENCE"]
READS_DIR=config["READS_DIR"]
PREFIX = config["PREFIX"]
SAMPLES_LIST = config["SAMPLE_LIST"]
CONTIGS_IGNORE = config["CONTIGS_IGNORE"]
SPECIES = config["SPECIES"]
NUM_CHRS =  config["NUM_CHRS"]
GFF_FILE = config['GFF_FILE']
samples_table = pd.read_csv(SAMPLES_LIST, header=None)
samples_list = list(samples_table.iloc[:,1])
SAMPLES = [os.path.splitext(x)[0] for x in samples_list]




localrules: create_file_log, simple_stats, plot_PCA

rule all:
    input:
        files_log,
        "FIGURES/" + PREFIX + ".pdf",
        "2_merged/" + PREFIX + ".smoove-counts.html",
        "6_metrics/"+ PREFIX + ".survivor.stats",
        "5_postprocessing/"+PREFIX + "_DUP_DEL_INV.vcf",
        "5_postprocessing/"+ PREFIX + "_BND.vcf",
        "5_postprocessing/"+ PREFIX + "_DUP_DEL_INV_table.tsv",
        "5_postprocessing/"+ PREFIX+ "_filtered_table.tsv",


# Create comma separated list of contigs to ignore
with open(CONTIGS_IGNORE, "r") as infile:
    content =  infile.read().splitlines()
    CONTIGS =  ",".join(content)

rule smoove_call:
    input:
        bam = os.path.join(READS_DIR, "{sample}.bam"),
        reference = REFERENCE,
    output:
        vcf = temp("1_call/{sample}-smoove.genotyped.vcf.gz"),
        idx = temp("1_call/{sample}-smoove.genotyped.vcf.gz.csi"),
        # t1 = temp(multiext("1_call/{sample,/^([^.]+)/}.split", ".bam", ".bam.csi", ".bam.orig.bam")),
        # t2 = "1_call/{sample}.sorted-lumpy-cmd.sh",
        # t3 = temp(multiext("1_call/{sample}.disc",".bam", ".bam.csi", ".bam.orig.bam")),
        # t4 = temp("1_call/{sample}.histo")
    message:
        'Rule {rule} processing'
    params:
        outdir = "1_call",
        contigs=CONTIGS,
        scripts_dir = os.path.join(workflow.basedir, "scripts/")
    conda:
        "envsmoove"
    group:
        'smoove_call'
    shell:
        """
        export PATH={params.scripts_dir}:$PATH
            
        smoove call --outdir {params.outdir} \
        --name {wildcards.sample} \
        --fasta {input.reference} \
        --duphold \
        --genotype \
        -p 1 \
        --excludechroms {params.contigs} \
        BAMS {input.bam}
        """

rule smoove_merge:
    input:
        reference = REFERENCE,
        vcfs = expand("1_call/{sample}-smoove.genotyped.vcf.gz", sample=SAMPLES),
        idx = expand("1_call/{sample}-smoove.genotyped.vcf.gz.csi", sample=SAMPLES),
        # t1 = expand("1_call/{sample}.split{ext}", ext=[".bam", ".bam.csi", ".bam.orig.bam"], sample=SAMPLES),
        # t2 = expand("1_call/{sample}.sorted-lumpy-cmd.sh", sample=SAMPLES),
        # t3 = expand("1_call/{sample}.disc{ext}", ext=[".bam", ".bam.csi", ".bam.orig.bam"], sample=SAMPLES),
        # t4 = expand("1_call/{sample}.histo", sample=SAMPLES)
    output:
        vcf = expand("2_merged/{prefix}.sites.vcf.gz", prefix=PREFIX),
        counts = expand("2_merged/{prefix}.smoove-counts.html", prefix=PREFIX)
    message:
        'Rule {rule} processing'
    params:
        outdir = "2_merged",
        name = PREFIX
    conda:
        "envsmoove"
    shell:
        """
        smoove merge --name {params.name} \
        --outdir {params.outdir} \
        -f {input.reference} \
        {input.vcfs}
        """

rule smoove_genotype:
    input:
        reference = REFERENCE,
        vcf = rules.smoove_merge.output.vcf,
        bam = os.path.join(READS_DIR, "{sample}.bam"),
    output:
        vcf = "3_genotyped/{sample}-smoove.genotyped.vcf.gz",
        idx = "3_genotyped/{sample}-smoove.genotyped.vcf.gz.csi"
    message:
        'Rule {rule} processing'
    params:
        outdir = "3_genotyped",
        name = "{sample}",
        scripts_dir = os.path.join(workflow.basedir, "scripts/")
    conda:
        "envsmoove"
    shell:
        """
export PATH={params.scripts_dir}:$PATH

smoove genotype -x \
--name {params.name} \
--outdir {params.outdir} \
--fasta {input.reference} \
--duphold \
-p 1 \
--vcf {input.vcf} \
{input.bam}
        """

rule smoove_paste:
    input:
        expand("3_genotyped/{sample}-smoove.genotyped.vcf.gz", prefix=PREFIX, sample=SAMPLES),
    output:
        temp("4_paste/{prefix}.smoove.square.vcf.gz")
    message:
        'Rule {rule} processing'
    conda:
        "envsmoove"
    params:
        outdir = "4_paste",
        name = PREFIX
    shell:
        """
        smoove paste --name {params.name} --outdir {params.outdir} {input}
        """


rule smoove_annotate:
    input:
        vcf = rules.smoove_paste.output,
        gff = GFF_FILE
    output:
        vcf = '5_postprocessing/{prefix}.smoove.square.annotate.vcf.gz',
    message:
        'Rule {rule} processing'
    conda:
        "envsmoove"
    group:
        'calling'
    shell:
        """
        smoove annotate --gff {input.gff} {input.vcf} | bgzip -c > {output.vcf}
        """

rule PCA:
    input:
        vcf = rules.smoove_annotate.output.vcf,
    output:
        eigenvec = "5_postprocessing/{prefix}.eigenvec",
        eigenval = "5_postprocessing/{prefix}.eigenval",
    message:
        'Rule {rule} processing'
    params:
        prefix= os.path.join("5_postprocessing",PREFIX),
        num_chrs = NUM_CHRS
    group:
        'calling'
    shell:
        """
        plink --vcf {input.vcf} --pca --double-id --out {params.prefix} --chr-set {params.num_chrs} --allow-extra-chr --threads 8
        """

rule plot_PCA:
    input:
        eigenvec = rules.PCA.output.eigenvec,
        eigenval = rules.PCA.output.eigenval,
        sample_list = SAMPLES_LIST
    output:
        "FIGURES/{prefix}.pdf"
    message:
        'Rule {rule} processing'
    params:
        rscript = os.path.join(workflow.basedir, "scripts/basic_pca_plot.R")
    # group:
    #     'calling'
    shell:
        """
        echo $CONDA_PREFIX
        export LD_LIBRARY_PATH=$CONDA_PREFIX/lib:$LD_LIBRARY_PATH
        echo $LD_LIBRARY_PATH
        Rscript {params.rscript} --eigenvec={input.eigenvec} --eigenval={input.eigenval} --output={output} --sample_list={input.sample_list}
        """

rule simple_stats:
    input:
        rules.smoove_annotate.output.vcf
    output:
        stats = "6_metrics/{prefix}.survivor.stats",
        chr_stats = "6_metrics/{prefix}.survivor.stats_CHR",
        support = "6_metrics/{prefix}.survivor.statssupport"
    message:
        'Rule {rule} processing'
    params:
        tmp = "6_metrics/{prefix}.stats.temp.vcf"
    log:
        err = "logs_slurm/simple_stats_{prefix}.err",
        out = "6_metrics/{prefix}.survivor.overall.stats"
    shell:
        """
        gzip -d -c {input} > {params.tmp}
        SURVIVOR stats {params.tmp} -1 -1 -1 {output.stats} 2> {log.err} 1> {log.out}

        rm {params.tmp}
        """

rule add_depth:
    input:
        rules.smoove_annotate.output.vcf
    output:
        dupdelinv = "5_postprocessing/{prefix}_DUP_DEL_INV.vcf",
        bnd = "5_postprocessing/{prefix}_BND.vcf"
    message:
        'Rule {rule} processing'
    params:
        script = os.path.join(workflow.basedir, "scripts/add_depth_field.py"),
        prefix = os.path.join("5_postprocessing",PREFIX)
    log:
        err = "logs_slurm/add_depth_{prefix}.err",
        out = "logs_slurm/add_depth_{prefix}.out"
    shell:
        'python {params.script} -v {input} -Q 30 -p {params.prefix} 2> {log.err} 1> {log.out}'

rule get_tsv:
    input:
        vcf = rules.add_depth.output.dupdelinv,
        samples_file = SAMPLES_LIST
    output:
        "5_postprocessing/{prefix}_DUP_DEL_INV_table.tsv"
    message:
        'Rule {rule} processing'
    params:
        script = os.path.join(workflow.basedir, "scripts/get_flat_file.py"),
    log:
        err = "logs_slurm/get_tsv_{prefix}.err",
        out = "logs_slurm/get_tsv_{prefix}.out"
    shell:
        """
        python {params.script} -v {input.vcf} -s {input.samples_file} -o {output}
        """
        
rule get_filtered_tsv:
    input:
        tsv = "5_postprocessing/{prefix}_DUP_DEL_INV_table.tsv"
    output:
        "5_postprocessing/{prefix}_filtered_table.tsv"
    message:
        'Rule {rule} processing'
    params:
        script = os.path.join(workflow.basedir, "scripts/filter_table.R"),
        qual = "30"
    log:
        err = "logs_slurm/get_tsv_{prefix}.err",
        out = "logs_slurm/get_tsv_{prefix}.out"
    shell:
        """
        Rscript {params.script} {input.tsv} {params.qual}  {output}
        """
