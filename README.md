# Population structural variantion calling

[Link to old repository](https://github.com/CarolinaPB/population-structural-var-calling-smoove/tree/single_run)

## First follow the instructions here:
[Step by step guide on how to use my pipelines](https://carolinapb.github.io/2021-06-23-how-to-run-my-pipelines/)  
Click [here](https://github.com/CarolinaPB/snakemake-template/blob/master/Short%20introduction%20to%20Snakemake.pdf) for an introduction to Snakemake

## ABOUT
This is a pipeline to perform structural variant calling in a population using Smoove. It also performs PCA. 
In addition to the VCF with the SVs, you also get a .tsv file with some summarized information on the SVs: it includes allele frequency per population, as well as smoove annotate annotation and depth fold change as described in [duphold](https://github.com/brentp/duphold):
> DHBFC: fold-change for the variant depth relative to bins in the genome with similar GC-content.  
> DHFFC: fold-change for the variant depth relative to Flanking regions.
The filtered table .tsv file is filtered for Qual > 30, Deletions: DHFFC 0/1  < 0.8 and Duplications: DHBFC (0/1) > 1.2.

#### Tools used:
- Smoove - SV calling and annotation of the variants from a GFF file
- Plink - perform PCA
- R - plot PCA
- SURVIVOR - basic SV stats
- Python  
  - PyVcf - add depth to vcf and create final table
  - bamgroupreads.py + samblaster - create bam files with split and discordant reads


| ![DAG](/dag.png) |
|:--:|
|*Pipeline workflow* |

#### Copy the pipeline:
Copy the pipeline to your directory, where you would run the pipeline.
```
cp -r /lustre/nobackup/WUR/ABGC/shared/pipelines_version2/population-structural-var-calling-smoove-version2 <directory where you want to save it to>
```

### Install conda if it is not yet installed
See installation instructions below. 

#### Activate environment:
Since the pipelines can take a while to run, it’s best if you use a screen session. By using a screen session, Snakemake stays “active” in the shell while it’s running, there’s no risk of the connection going down and Snakemake stopping. `screen -S <name of session>` to start a new screen session and `screen -r <name of session` to reattach to the screen session. In the screen session activate the conda environment. The pipelines uses a extra conda environment with smoove. The environment needs to be activated before running the snakemake pipeline.

```
conda activate /lustre/nobackup/WUR/ABGC/shared/pipelines_version2/envs/population-structural-var-calling-smoove-version2
```
The pipeline uses a extra conda environment with smoove. The environment needs to be activated before running the snakemake pipeline.
```
conda create -n envsmoove  --clone /lustre/nobackup/WUR/ABGC/shared/pipelines_version2/envs/envsmoove
```
#### Edit config.yaml with the paths to your files
Configure your paths, but keep the variable names that are already in the config file. See below for more details.

#### Run the pipeline on the cluster:
First test the pipeline with a dry run: `snakemake -np`. This will show you the steps and commands that will be executed. Check the commands and file names to see if there’s any mistake. If all looks ok, you can now run your pipeline


To run the pipeline, run the following command:
```
snakemake -j 8 --cluster-config cluster.yaml --cluster "sbatch --mem={cluster.mem} --time {cluster.time} --cpus-per-task {cluster.threads} --job-name={cluster.name} --output={cluster.output} --error={cluster.error}" --use-conda

```

### Edit config.yaml with the paths to your files
```
OUTDIR: /path/to/output 
READS_DIR: /path/to/bams/dir/ # don't add the bam files, just the directory where they are processed (/processed_reads/ from mapping pipeline)
SAMPLE_LIST: /path/to/file
REFERENCE: /path/to/assembly
CONTIGS_IGNORE: /path/to/file
SPECIES: <species_name>
PREFIX: <output name>
NUM_CHRS: <number of chromosomes>
GFF_FILe: /path/to/gff(3)file (from the same resource as your reference (e.g. ensembl))
```

- OUTDIR - directory where snakemake will run and where the results will be written to
- READS_DIR - path to the directory that contains mapped bam files
- SAMPLE_LIST - three column csv with the sample name, name of the bam files to use in the second column and the name of the corresponding population on the third column. These bams should all be in the same directory (READS_DIR)
- Example: 
> sample1,sample1.sorted.bam,Pop1   
> sample2,sample2.sorted.bam,Pop1   
> sample3,sample3.sorted.bam,Pop2   
> sample4,sample4.sorted.bam,Pop2  

 Use the name of the bam file without the .bam extension as the sample name. Ex: from sample1.bam to sample1

- REFERENCE - path to the assembly file
- CONTIGS_IGNORE - contigs to be excluded from SV calling (usually the small contigs)
- SPECIES - species name (has no influence on any tools or on the output)
- PREFIX - prefix for the created files
- NUM_CHRS - number of chromosomes for your species (necessary for plink). ex: 38 for pig
- GFF_FILe - path to annotation gff(3) file, can be zipped .gff3.gz


If you want the results to be written to this directory (not to a new directory), comment out or remove
```
OUTDIR: /path/to/outdir
```
### Error Optparse
If you get an error about R package optparse. Enter the R environment by writing R and clicking enter. Install the packages:
`install.packages("optparse")`

## RESULTS
* **<run_date>_files.txt** Dated file with an overview of the files used to run the pipeline (for documentation purposes)
* **2_merged** 
  * {prefix}.smoove-counts.html - shows a summary of the number of reads before and after filtering 
* **5_postprocessing** directory that contains the final VCF file containing the structural variants found. This file has been annotated with smoove annotate
  * {prefix}.smoove.square.annotate.vcf.gz - Final VCF - with annotation, not filtered for quality
  * {prefix}.nosex, {prefix}.log, {prefix}.eigenvec, {prefix}.eigenval - output files from the PCA
  * {prefix}_DUP_DEL_INV_table.tsv - table with the most important information extracted from the VCF. Contains information about the SV, allele frequency for each population, annotation and depth information. The variants have been filtered with Minimum Quality score = 30
  *{prefix}_filtered_table.tsv - {prefix}_DUP_DEL_INV_table.tsv filtered for deletions: DHFFC 0/1  < 0.8 and duplications: DHBFC (0/1) > 1.2 to limit the number of false positives. 
  * {prefix}_DUP_DEL_INV.vcf - vcf file with annotated duplications, deletions and inversions. It has been filtered with Minimum Quality score = 30 and the DEPTH* field was added
  * {prefix}_BND.vcf - vcf file with variants annotated with BND
* **6_metrics** directory that contains general stats about the number of SVs found
* **FIGURES** directory that contains the PCA plot 

What you do with the results from this structural variant calling pipeline depends on your research question: a possible next step would be to explore the **{prefix}_DUP_DEL_INV_table.tsv** file and look at the largest SVs found (sort by _SVLEN_) or at a specific effect in the ANNOTATION column.  


***
*The **DEPTH** field in the vcf has six fields, corresponding to the average depth across all samples.
```
DEPTH=(DHBFC_1/1, DHBFC_0/1, DHBFC_0/0, DHFFC_1/1, DHFFC_0/1, DHFFC_0/0)
```
Depth fold change as described in [duphold](https://github.com/brentp/duphold):
> DHBFC: fold-change for the variant depth relative to bins in the genome with similar GC-content.  
> DHFFC: fold-change for the variant depth relative to Flanking regions.

These fields are also in the `{prefix}_DUP_DEL_INV_table.tsv` file


